# Last Visited Pages
### Objective:
Last Visited Pages is a Module that Keep tracks of all the routes visited by a User, and provides Block Plugin to display Title, URL and Time each route was visited.

### Features:
This Module will show Proficiency is following areas of Module Development.

* Plugin Ins
* Blocks
* Configurations Forms
* Event Subscriber
* Routing
* Hook Help
* Admin Config Options for Block Factory setting
* Cache Invalidation

### Set Up:
1. To Setup, Like any Drupal 9 module Just Place the Module Files in Module Directory
2. Enable the module "Last Visited Pages" through Extends Admin Tab
3. Once Enabled a block **Last Visited Pages** would be Available in the Block Layout.
4. Place the Block on the areas you wish you display recently visited places (ex: sidebar, footer)
5. You can configure the blocks to show the number of results you wish to see.
6. Additionally the blocks could be configured from Drupal Configuration Page under the User Interface Header.


