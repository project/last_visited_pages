<?php

/**
 * @file
 * Contains \Drupal\last_visited_pages\LastVisitedPagesSubscriber.
 */

namespace Drupal\last_visited_pages;

use Drupal\Console\Bootstrap\Drupal;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Subscribes to the kernel request event to completely obliterate the default content.
 *
 * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
 *   The event to process.
 */
class LastVisitedPagesSubscriber implements EventSubscriberInterface {

  /**
   * Redirects the user when they're requesting our nearly blank page.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The response event.
   */
  public function checkForPlaces(GetResponseEvent $event) {
      // Grab the saved Last Visited Pages.
    $places = \Drupal::state()->get('recent.places') ?: array();
    $request = \Drupal::request();
    $route_match = \Drupal::routeMatch();
    $title = \Drupal::service('title_resolver')->getTitle($request, $route_match->getRouteObject());

    if(is_array($title)) {
      $title = array_key_exists('#markup', $title) ? $title['#markup'] : $title;
    } elseif (!is_null($title)) {
      $title = strip_tags($title);
    }

      // Save Last Visited Pages only for prominent destinations with titles.
    if(!is_null($title)) {
      $uid = \Drupal::currentUser()->id();
      $path = \Drupal::service('path.current')->getPath();
      $uid = \Drupal::currentUser()->id();

      $file_insert_id = \Drupal::database()->insert('last_visited_pages')
      ->fields([
        'title',
        'path',
        'timestamp',
        'uid'
      ])
      ->values(array(
        $title,
        $path,
        REQUEST_TIME,
        $uid
      ))
      ->execute();

      $result_query_count = \Drupal::database()->select('last_visited_pages', 'n')
      ->fields('n', array('id'))
      ->condition('uid', $uid, '=')
      ->range(20, 100)
      ->orderBy('timestamp', 'DESC');

      $all_result_count = $result_query_count
      ->execute()
      ->fetchCol();

      if(!empty($all_result_count)) {
        $result_query_count = \Drupal::database()->delete('last_visited_pages')
        ->condition('id', $all_result_count, 'IN')
        ->execute();
      }

    }
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents(){
    $events[KernelEvents::REQUEST][] = array('checkForPlaces');
    return $events;
  }
}